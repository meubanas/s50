import {Fragment, useEffect, useState} from 'react'
// import coursesData from '../data/coursesData'
import coursesData from '../data/coursesData'
import CourseCard from '../components/CourseCard'

export default function Courses(){
	
	// console.log(coursesData)
	// console.log(coursesData[0])

const [course, setCourse] = useState([])

useEffect(() => {
	fetch('http://localhost:4000/courses/')
	.then(res => res.json())
	.then(data => {
		console.log(data)
	
	setCourse(data.map(course => {
	return (
		<CourseCard key = {course.id} courseProp = {course}/>
	)
}))


	})
}, [])

const courses = coursesData.map(course => {
	return (
		<CourseCard key = {course.id} courseProp = {course}/>
	)
})


	return(
	<Fragment>	
		<h1>Courses</h1>
		{course}
	</Fragment>	
	)
}