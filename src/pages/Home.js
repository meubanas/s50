import {Fragment} from 'react';
import Banner from '../components/Banner';
// import CourseCard from '../components/CourseCard';
import Highlights from '../components/Highlights';


export default function Home (){

	const data = {
		header:"Zuitt Coding Bootcamp",
		content:"Opportunities are for everyone and everywhere", 
		btnType:"primary",
		buttonContent:"Enroll Now!",
		destination: "/register"
	}




	return(
		<Fragment>
			<Banner data={data} />
			<Highlights/>
			{/*<CourseCard/>*/}
		</Fragment>
	)
}
