import { useState, useEffect, useContext } from 'react';
import { Form, Button } from 'react-bootstrap';
import { Redirect } from 'react-router-dom'
import UserContext from '../UserContext'
import Swal from 'sweetalert2'

export default function Register() {

const {user, setUser} = useContext(UserContext)

// State hooks to store the values of the input fields
const [firstName, setfirstName] = useState('');
const [lastName, setlastName] = useState('');
const [email, setEmail] = useState('');
const [mobileNumber, setmobileNumber] = useState('');
const [password1, setPassword1] = useState('');
const [password2, setPassword2] = useState('');
// State to determine whether submit button is enabled or not
const [isActive, setIsActive] = useState('');



// Check if values are successfully binded
	console.log(firstName);
	console.log(lastName);
	console.log(email);
	console.log(mobileNumber);
	console.log(password1);
	console.log(password2);

	function registerUser(e) {

		// Prevents page redirection via form submission
		e.preventDefault();

		fetch('http://localhost:4000/users/checkEmail', {
			method: 'POST',
			headers: {
				'Content-Type' : 'application/json'
			},
			body: JSON.stringify({
				email: email,
			})
		}).then (res => res.json())
		.then(data => {
			if (data === true) {
				Swal.fire({
					title: 'Duplicate email found.',
					icon: 'error',
					text: 'Please provide a different email'
				})
			} else {

			fetch('http://localhost:4000/users/register', {
				method: 'POST',
				headers: {
					'Content-Type' : 'application/json'
				},
				body: JSON.stringify({
					firstName: firstName,
					lastName: lastName,
					email: email,
					password: password1,
					mobileNo: mobileNumber
				})
			})
			.then(res => res.json ())
			.then (data => {
				if (data === true) {
					Swal.fire({
						title: 'Registration successful!',
						icon: 'success',
						text: 'Welcome to Zuitt!'
					}).then(redirect => {
						window.location = "/login"
					})		
				}
			})

			}
		})


		//Clears the input fields
		setfirstName('');
		setlastName('');
		setEmail('');
		setmobileNumber('');
		setPassword1('');
		setPassword2('');

		Swal.fire({
                title: 'Thank you for registering!',
                icon: 'success',
                text: 'Welcome to Zuitt'
            })
	} 





useEffect(() => {

    // Validation to enable submit button when all fields are populated and both passwords match
    if((firstName !== '' && lastName !== '' && email !== '' && password1 !== '' && password2 !== '') && (password1 === password2)){
        setIsActive(true);
    } else {
        setIsActive(false);
    }

}, [firstName, lastName, email, mobileNumber, password1, password2]);





    return (
    	(user.id !== null) ? 
        <Redirect to="/Login" />
        :
    	

        <Form onSubmit={(e)=>registerUser(e)}>

        	<Form.Group controlId="firstName">
                <Form.Label>First Name</Form.Label>
                <Form.Control 
	                type="firstName" 
	                placeholder="Enter first name"
	                value={firstName} 
	                onChange={e => setfirstName(e.target.value)} 
	                required
                />
            </Form.Group>

            <Form.Group controlId="lastName">
                <Form.Label>Last Name</Form.Label>
                <Form.Control 
	                type="lastName" 
	                placeholder="Enter last name"
	                value={lastName} 
	                onChange={e => setlastName(e.target.value)} 
	                required
                />
            </Form.Group>

            <Form.Group controlId="userEmail">
                <Form.Label>Email address</Form.Label>
                <Form.Control 
	                type="email" 
	                placeholder="Enter email"
	         		value={email} 
	                onChange={e => setEmail(e.target.value)} 
	                required
                />
                <Form.Text className="text-muted">
                    We'll never share your email with anyone else.
                </Form.Text>
            </Form.Group>

            <Form.Group controlId="mobileNumber">
                <Form.Label>Mobile Number</Form.Label>
                <Form.Control 
	                type="mobileNumber" 
	                placeholder="Enter mobile number"
	                value={mobileNumber} 
	                onChange={e => setmobileNumber(e.target.value)} 
	                required
                />
            </Form.Group>


            <Form.Group controlId="password1">
                <Form.Label>Password</Form.Label>
                <Form.Control 
	                type="password" 
	                placeholder="Password"
	                value={password1} 
	                onChange={e => setPassword1(e.target.value)} 
	                required
                />
            </Form.Group>

            <Form.Group controlId="password2">
                <Form.Label>Verify Password</Form.Label>
                <Form.Control 
	                type="password" 
	                placeholder="Verify Password"
	                value={password2} 
	                onChange={e => setPassword2(e.target.value)} 
	                required
                />
            </Form.Group>

				{/* conditionally render submit button based on isActive state */}
    	    { isActive ? 
    	    	<Button className = "my-3" variant="primary" type="submit" id="submitBtn">
    	    		Submit
    	    	</Button>
    	        : 
    	        <Button className = "my-3" variant="danger" type="submit" id="submitBtn" disabled>
    	        	Submit
    	        </Button>
    	    }

    	    

        </Form>
    )

}
